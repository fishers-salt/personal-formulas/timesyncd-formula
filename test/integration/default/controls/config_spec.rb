# frozen_string_literal: true

control 'timesyncd configuration' do
  title 'should match desired lines'

  describe file('/etc/systemd/timesyncd.conf') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include '#  This file is part of systemd' }
  end
end
