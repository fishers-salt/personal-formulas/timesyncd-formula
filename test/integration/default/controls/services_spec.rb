# frozen_string_literal: true

control 'timesyncd service' do
  impact 0.5
  title 'should be running and enabled'

  describe service('systemd-timesyncd') do
    it { should be_enabled }
  end
end
